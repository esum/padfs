# README

Installation
------------

Installez les dépendances via pip :

```Bash
pip3 install request
pip3 install fusepy
```

Récupérez la clef de l'API de votre serveur etherpad.

Créez un groupe de pad en allant ici https://{padserver}/api/1/createGroup?apikey={apikey}.

Créez le fichier de secrets :

```Bash
cp src/secrets_example.py src/secrets.py
```

Puis éditez `src/secrets.py` pour qu'il contienne la clef de l'API et votre identifiant de groupe.


Utilisation
-----------

```Bash
padfs.py [-h|--help] [-k|--key apikey] [-g|--group groupID] [-v|--verbose] [--host padserver] target
```

 * `--help` : afficher l'aide.
 * `--key` : Clef de l'API. (valeur par défaut dans `secrets.py`)
 * `--group` : Identifiant du groupe. (valeur par défaut dans `secrets.py`)
 * `--verbose` : Afficher plus d'informations de débogage.
 * `--host` : Adresse du serveur de pad. (valeur par défaut `pad.crans.org`)
 * `target` : Point de montage du système de fichiers.
