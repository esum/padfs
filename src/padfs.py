from sys import argv, exit
from time import time
import json
import base64
from fuse import FUSE, Operations, LoggingMixIn, FuseOSError
from errno import ENOENT, ENOTEMPTY
from stat import S_IFDIR, S_IFLNK, S_IFREG
import requests
import logging
import argparse
import secrets


class PAD(LoggingMixIn, Operations):

    def __init__(self, host, key, group, path='.'):
        self.host = host
        self.key = key
        self.group = group
        requests.get('https://{host}/api/1/createGroupPad'.format(host=self.host), params = {'apikey': self.key, 'groupID': self.group, 'padName': base64.urlsafe_b64encode('/'.encode('utf-8')).decode('utf-8')})

    def chmod(self, path, mode):
        return 0

    def chown(self, path, uid, gid):
        return 0

    def create(self, path, mode):
        r = requests.get('https://{host}/api/1/createGroupPad'.format(host=self.host), params = {'apikey': self.key, 'groupID': self.group, 'padName': base64.urlsafe_b64encode(path.encode('utf-8')).decode('utf-8')})
        if r.ok:
            return 0
        return 1

    def unlink(self, path):
        r = requests.get('https://{host}/api/1/deletePad'.format(host=self.host), params = {'apikey': self.key, 'padID': self.group + '$' + base64.urlsafe_b64encode(path.encode('utf-8')).decode('utf-8')})
        if r.ok:
            return 0

    def getattr(self, path, fh=None):
        if path == '/':
            return dict(st_mode=0o777 | S_IFDIR, st_nlink=1, st_size=0, st_ctime=time(), st_mtime=time(), st_atime=time())
        r = requests.get('https://{host}/api/1/listPads'.format(host=self.host), params = {'apikey': self.key, 'groupID': self.group})
        if r.ok:
            t = json.loads(r.text)
            files = t["data"]["padIDs"]
            if self.group + '$' + base64.urlsafe_b64encode(path.encode('utf-8')).decode('utf-8') in files:
                r = requests.get('https://{host}/api/1/getText'.format(host=self.host), params = {'apikey': self.key, 'padID': self.group + '$' + base64.urlsafe_b64encode(path.encode('utf-8')).decode('utf-8')})
                if r.ok:
                    t = json.loads(r.text)
                    s = len(base64.b64decode(t["data"]["text"].encode('ascii')))
                    return dict(st_mode=0o777 | S_IFREG, st_nlink=1, st_size=s, st_ctime=time(), st_mtime=time(), st_atime=time())
            elif self.group + '$' + base64.urlsafe_b64encode((path+'/').encode('utf-8')).decode('utf-8') in files:
                return dict(st_mode=0o777 | S_IFDIR, st_nlink=1, st_size=0, st_ctime=time(), st_mtime=time(), st_atime=time())
            else:
                raise FuseOSError(ENOENT)
        else:
            raise FuseOSError(ENOENT)

    def mkdir(self, path, mode):
        self.create(path+'/', mode)
        return

    def read(self, path, size, offset, fh):
        r = requests.get('https://{host}/api/1/getText'.format(host=self.host), params = {'apikey': self.key, 'padID': self.group + '$' + base64.urlsafe_b64encode(path.encode('utf-8')).decode('utf-8')})
        if r.ok:
            t = json.loads(r.text)
            return base64.b64decode(t["data"]["text"].encode('ascii'))

    def readdir(self, path, fh):
        r = requests.get('https://{host}/api/1/listPads'.format(host=self.host), params = {'apikey': self.key, 'groupID': self.group})
        if r.ok:
            t = json.loads(r.text)
            files = t["data"]["padIDs"]
            if path != '/':
                if self.group + '$' + base64.urlsafe_b64encode((path+'/').encode('utf-8')).decode('utf-8') not in files:
                    return []
                path += '/'
            res = ['.', '..']
            for i in range(len(files)):
                file = base64.urlsafe_b64decode(files[i][len(self.group)+1:].encode('utf-8')).decode('utf-8')
                print(file)
                if file.startswith(path):
                    file = file[len(path):]
                    if file.count('/') == 0 and file:
                        res.append(file)
                    elif file.count('/') == 1 and file.endswith('/'):
                        res.append(file[:-1])
            return res

    readlink = None

    def rename(self, old, new):
        r = requests.get('https://{host}/api/1/movePad'.format(host=self.host), params = {'apikey': self.key, 'sourceID': self.group + '$' + base64.urlsafe_b64encode(old.encode('utf-8')).decode('utf-8'), 'destinationID': base64.urlsafe_b64encode(new.encode('utf-8')).decode('utf-8'), 'force': 'true'})
        if r.ok:
            return 0
        return 1

    def destroy(self, path):
        return

    def rmdir(self, path):
        r = requests.get('https://{host}/api/1/listPads'.format(host=self.host), params = {'apikey': self.key, 'groupID': self.group})
        if r.ok:
            t = json.loads(r.text)
            files = t["data"]["padIDs"]
            if path != '/':
                if self.group + '$' + base64.urlsafe_b64encode((path+'/').encode('utf-8')).decode('utf-8') not in files:
                    return []
                path += '/'
            for i in range(len(files)):
                file = files[i] = base64.urlsafe_b64decode(files[i][len(self.group)+1:].encode('utf-8')).decode('utf-8')
                if file.startswith(path):
                    if len(file) > len(path):
                        raise FuseOSError(ENOTEMPTY)
            if path in files:
                self.unlink(path)

    symlink = None
    truncate = None
    utimens = None

    def flush(self, path, fh):
        return

    def release(self, path, fh):
        return

    def write(self, path, data, offset, fh):
        r = requests.get('https://{host}/api/1/setText'.format(host=self.host), params = {'apikey': self.key, 'padID': self.group + '$' + base64.urlsafe_b64encode(path.encode('utf-8')).decode('utf-8'), 'text': base64.b64encode(data).decode('ascii')})
        return len(data)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '--key', help='Clé de l\'api.', type=str, default=secrets.key)
    parser.add_argument('-g', '--group', help='Groupe des pads.', type=str, default=secrets.group)
    parser.add_argument('-v', '--verbose', help='Affichage des logs.', action='store_true')
    parser.add_argument('--host', help='Nom de domaine de l\'etherpad.', type=str, default='pad.crans.org')
    parser.add_argument('target', help='Point de montage.', type=str, nargs=1)
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    fuse = FUSE(PAD(args.host, args.key, args.group), args.target[0], foreground=True, nothreads=True)

